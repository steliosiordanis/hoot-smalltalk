package Hoot.Tests;

import org.junit.*;
import Hoot.Examples.*;
import Hoot.Runtime.Faces.Logging;
import Hoot.Runtime.Maps.ClassPath;

/**
 * Confirms proper operation of provided Hoot examples.
 *
 * @author nik <nikboyd@sonic.net>
 * @see "Copyright 2010,2019 Nikolas S Boyd."
 * @see "Permission is granted to copy this work provided this copyright statement is retained in all copies."
 * @see <a href="https://gitlab.com/hoot-smalltalk/hoot-smalltalk/tree/master/LICENSE.txt">LICENSE for more details</a>
 */
public class ExamplesTest implements Logging {

    private static final String SourceFolder = "../code-samples/src/main/Hoot";
    private static final String TargetFolder = "../libs-samples/target/classes";
    private static final String ClassFolder  = "../libs-hoot/target/classes";
    private static final String[] BasePaths = { SourceFolder, TargetFolder, ClassFolder, };

    @BeforeClass public static void prepare() { ClassPath.loadBasePaths(BasePaths); }

    @Test public void streamsTest() { StreamsTest.main(); }
    @Test public void threadTest() { ThreadTest.main(); }
    @Test public void collsTest() { CollectionsTest.main(); }
    @Test public void magsTest() { MagnitudesTest.main(); }
    @Test public void hanoiTest() { SimpleHanoi.main(); }
    @Test public void helloWorld() { HelloWorld.main(); }
    @Test public void litsTest() { LiteralsTest.main(); }
    @Test public void intsTest() { IntegersTest.main(); }
    @Test public void geoTest() { GeometryTest.main(); }
    @Test public void sticTest() { SticBenchmark.main(); }

} // ExamplesTest
