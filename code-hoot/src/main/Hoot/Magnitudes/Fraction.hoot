@Notice :'Copyright 2010,2019 Nikolas S Boyd.
Permission is granted to copy this work provided this copyright statement is retained in all copies.
See https://gitlab.com/hoot-smalltalk/hoot-smalltalk/tree/master/LICENSE.txt for more details.'!

Java Lang Math import.
Java Util List import.

Hoot Magnitudes Number import.
Hoot Runtime Names Primitive import.

Smalltalk Magnitudes Ordinal import.
Smalltalk Magnitudes Numeric import.
Smalltalk Magnitudes Fractional import.

Rational subclass: Fractional! Fraction. "An exact representation for rational numbers."

Fraction class members:
[
    Rational! coerce: aNumber [ ^aNumber asFraction ]
    Fraction! numerator: Ordinal! upper denominator: Ordinal! lower [
        ^self rationalized: (Integer <- upper) over: (Integer <- lower) ]

    @Primitive! Fraction! rationalized: Number! upper over: Number! lower [
        "Returns a new rationalized Fraction for the supplied arguments."
    	SmallInteger! num := upper truncated asSmallInteger.
        SmallInteger! den := lower truncated asSmallInteger.
        ^self rationalized: (Primitive rationalize: num intValue : den intValue) ]

    @Primitive! Fraction! rationalized: Java Lang Integer! ... parts [ 
        List? Java Lang Integer! ps := Utils wrap: parts. ^Fraction basicNew: (ps get: 0) : (ps get: 1) ]
]

Fraction members:
[
    "Used for numeric coercions."
    @Static! SmallInteger! Generality := 40.

    "The numerator of a fraction."
    Integer! numerator := 0.

    "The denominator of a fraction."
    Integer! denominator := 1.

    Fraction []
    Fraction: Integer! upper [ numerator := upper. ]
    Fraction: Int! upper : Int! lower [ self : (SmallInteger from: upper) : (SmallInteger from: lower). ]
    Fraction: Integer! upper : Integer! lower [ numerator := upper. denominator := lower. ]
]

Fraction members: "accessing"
[
    Integer! numerator [ ^numerator ]
    Integer! denominator [ ^denominator ]
]

Fraction members: "arithmetic"
[
    Rational! - Rational! aNumber [
    	denominator = aNumber denominator ifTrue: [
            ^self class numerator: numerator - aNumber numerator denominator: denominator ].

        ^self class numerator: (self crossDiff: aNumber) denominator: (self underProduct: aNumber) ]

    Rational! + Rational! aNumber [
    	denominator = aNumber denominator ifTrue: [
            ^self class numerator: numerator + aNumber numerator denominator: denominator ].

        ^self class numerator: (self crossSum: aNumber) denominator: (self underProduct: aNumber) ]

    Rational! * Rational! aNumber [
    	^self class numerator: (self overProduct: aNumber) denominator: (self underProduct: aNumber) ]

    Rational! / Rational! aNumber [
    	^self class numerator: (self crossProduct: aNumber) denominator: (aNumber crossProduct: self) ]

    Number! + Numeric! aNumber [ ^self + (Rational <- aNumber asRational) ]
    Number! * Numeric! aNumber [ ^self * (Rational <- aNumber asRational) ]
    Number! / Numeric! aNumber [ ^self / (Rational <- aNumber asRational) ]
]

Fraction members: "converting"
[
    asFraction [ ^self ]
    Float! asFloat [ ^Float from: self primitiveFloat ]
    Double! asFloatD [ ^Double from: self primitiveDouble ]

    Integer! generality [ ^Generality ]
    @Primitive! Java Lang Number! elementaryNumber [ ^self primitiveDouble ]
    @Primitive! Java Lang Long! primitiveLong [
        Java Lang Double! value := self primitiveDouble.
        (value < 0) ifTrue: [ ^Long <- (Math ceil: value) ].
        ^Long <- (Math floor: value) ]

    @Primitive! Java Lang Float! primitiveFloat [ ^numerator primitiveFloat / denominator primitiveFloat ]
    @Primitive! Java Lang Double! primitiveDouble [ ^numerator primitiveDouble / denominator primitiveDouble ]

    Fixed! asFixedPoint: Ordinal! scale [ ^self scaledAt: Integer <- scale ]
    Fixed! asScaledDecimal: Ordinal! scale [ ^self scaledAt: Integer <- scale ]

    Fixed! scaledZero [ ^Fixed basicNew: numerator : denominator ]
    Fixed! scaledAt: Integer! scale [ ^Fixed basicNew: numerator : denominator : scale ]
]

Fraction members: "mathematics"
[
    negated [ "the arithmetic inverse of this fraction"
     	^self class numerator: numerator negated denominator: denominator ]

    reciprocal [ "the multiplicative inverse of this fraction"
     	^self class numerator: denominator denominator: numerator ]
]

Fraction members: "printing"
[
    @Primitive! String! printString [ ^String from: numerator printString + ' / ' + denominator printString ]
]
