@Notice :'Copyright 2010,2019 Nikolas S Boyd.
Permission is granted to copy this work provided this copyright statement is retained in all copies.
See https://gitlab.com/hoot-smalltalk/hoot-smalltalk/tree/master/LICENSE.txt for more details.'!

Hoot Runtime Faces IntegerValue import.
Hoot Runtime Names Primitive import.

Hoot Magnitudes Integer import.
Smalltalk Magnitudes Scalar import.
Smalltalk Magnitudes Ordinal import.
Smalltalk Magnitudes Numeric import.

Integer subclass: SmallInteger.
"An integer value, especially for a literals. Manages flyweight instances of 32 bits precision."

SmallInteger class members: "converting value types"
[
    SmallInteger! IntegerFrom: Java Lang Integer! intValue "a new integer" [ ^SmallInteger from: intValue ]
    @Primitive! Java Lang Integer! intFrom: Integer! anInteger [ ^anInteger intValue ]
    SmallInteger! coerce: aNumber [ ^SmallInteger from: aNumber primitiveInteger ]
]

SmallInteger class members: "accessing flyweights"
[
    "Used for numeric coercions."
    @Static! SmallInteger! Generality.

    "Caches the flyweight instances."
    @Static! IntegerValue Cache! FlyweightCache.

    "Instantiates the flyweight SmallInteger instances."
    @Primitive! @Protected! Void! initialize [
        "this method MUST be primitive to prevent a runtime recursion error!"
        FlyweightCache := IntegerValue Cache basicNew. "order matters!"

        FlyweightCache cache: (SmallInteger basicNew: 0).
        FlyweightCache cache: (SmallInteger basicNew: 1).
        FlyweightCache cache: (SmallInteger basicNew: 2).
        FlyweightCache cache: (SmallInteger basicNew: 5).
        FlyweightCache cache: (SmallInteger basicNew: 10).
        FlyweightCache cache: (SmallInteger basicNew: 20).
        FlyweightCache cache: (SmallInteger basicNew: 30).
        FlyweightCache cache: (SmallInteger basicNew: 40).
        FlyweightCache cache: (SmallInteger basicNew: 50).
        FlyweightCache cache: (SmallInteger basicNew: 60).
        FlyweightCache cache: (SmallInteger basicNew: 70).

        Generality := FlyweightCache getCached: 10.
    ]

    IntegerValue Cache! flyweights [ ^FlyweightCache ]
]

SmallInteger class members: "characterizing values"
[
    SmallInteger! maximum [ ^SmallInteger from: Primitive elementaryMaxInteger ]
    SmallInteger! minimum [ ^SmallInteger from: Primitive elementaryMinInteger ]

    SmallInteger! radix [ ^Duality ]
    SmallInteger! precision [ ^32 ]
    SmallInteger! generality [ ^Generality ]
]

SmallInteger members: "caching flyweights"
[
    @Public! @Static! SmallInteger! Zero := 0.
    @Public! @Static! SmallInteger! Unity := 1.
    @Public! @Static! SmallInteger! Duality := 2.
    @Public! @Static! SmallInteger! Negativity := 0-1.

    @Primitive! @Static! from: Java Lang Int! intValue [
       "a SmallInteger derived from a primitive Integer"

        "if outside cache bounds ..."
        (SmallInteger type flyweights covers: intValue) ifFalse: [
            ^SmallInteger basicNew: intValue ].

        "if not yet cached ..."
        (SmallInteger type flyweights hasCached: intValue) ifFalse: [
            ^SmallInteger type flyweights cache: (SmallInteger basicNew: intValue) ].

        ^SmallInteger type flyweights getCached: intValue
    ]
]

SmallInteger members: "constructing values"
[
    "Holds a primitive integer value."
    @Primitive! @Protected! Java Lang Int! cachedValue := 0.

    @Protected! SmallInteger [ super. ]
    @Protected! SmallInteger: Java Lang Int! value [ self. cachedValue := value. ]
]

SmallInteger members: "arithmetic - final"
[
    @Primitive! @Final! + SmallInteger! aNumber [
        ^SmallInteger from: cachedValue + aNumber intValue ]

    @Primitive! @Final! - SmallInteger! aNumber [
        ^SmallInteger from: cachedValue - aNumber intValue ]

    @Primitive! @Final! * SmallInteger! aNumber [
        ^SmallInteger from: cachedValue * aNumber intValue ]

    @Primitive! @Final! Number! / SmallInteger! aNumber [
        aNumber primitiveInteger == 0 ifTrue: [ self zeroDivide. ^self ].
        ^Fraction type numerator: self denominator: aNumber ]
]

SmallInteger members: "arithmetic"
[
    Integer! + Numeric! aNumber [ ^(LargeInteger from: (
        (Number <- aNumber) asInteger asBigInteger add: self asBigInteger)) narrowGenerality ]

    Integer! * Numeric! aNumber [ ^(LargeInteger from: (
        (Number <- aNumber) asInteger asBigInteger multiply: self asBigInteger)) narrowGenerality ]

    / aNumber [ ^self // aNumber asSmallInteger ]
    // Numeric! aNumber [ ^self // (Number <- aNumber) asSmallInteger ]

    @Primitive! / Numeric! aNumber [
        Number! n := Number <- aNumber.
        n isZero primitiveBoolean ifTrue: [ self zeroDivide. ^self ].
        ^self divideWith: n ]

    @Primitive! // SmallInteger! aNumber [
        aNumber primitiveInteger == 0 ifTrue: [ self zeroDivide. ^self ].
        ^SmallInteger from: (self primitiveInteger / aNumber intValue) ]

    @Primitive! \\ SmallInteger! aNumber [
        aNumber primitiveInteger == 0 ifTrue: [ self zeroDivide. ^self ].
        ^SmallInteger from: (self primitiveInteger % aNumber intValue) ]
]

SmallInteger members: "comparing values - final"
[
    @Primitive! @Final! Boolean! = SmallInteger! aNumber [
        ^Boolean from: self primitiveInteger == aNumber intValue ]

    @Primitive! @Final! Boolean! < SmallInteger! aNumber [
        ^Boolean from: self primitiveInteger < aNumber intValue ]

    @Primitive! @Final! Boolean! > SmallInteger! aNumber [
        ^Boolean from: self primitiveInteger > aNumber intValue ]
]

SmallInteger members: "comparing values"
[
    Boolean! = Magnitude! aNumber [ ^self = (SmallInteger <- aNumber) ]
    Boolean! = Numeric! aNumber [ ^(Number <- aNumber) asSmallInteger = self ]
    Boolean! < Rational! aNumber [ ^aNumber asSmallInteger > self ]
    Boolean! > Rational! aNumber [ ^aNumber asSmallInteger < self ]
]

SmallInteger members: "converting values"
[
    @Static! SmallInteger! from: Numeric! aNumber [ ^SmallInteger from: aNumber asInteger intValue ]
    @Primitive! Java Lang Short! primitiveShort [ ^self elementaryInteger shortValue ]

    Java Lang Byte! primitiveByte [ ^self elementaryInteger byteValue ]
    Java Lang Char! primitiveCharacter [ ^char <- self primitiveInteger ]
    Java Lang Int! primitiveInteger [ ^cachedValue ]
    Java Lang Integer! elementaryInteger [ ^cachedValue ]
    Java Lang Int! asPrimitive [ ^self primitiveInteger ]
    Java Lang Long! primitiveLong [ ^self elementaryInteger longValue ]
    Java Lang Float! primitiveFloat [ ^self elementaryInteger floatValue ]
    Java Lang Double! primitiveDouble [ ^self elementaryInteger doubleValue ]
    Java Lang Number! elementaryNumber [ ^self elementaryInteger ]

    asSmallInteger [ ^self ]
    String! asString [ ^self printString ]
    FastInteger! asFastInteger [ ^FastInteger fromInteger: self ]

    generality [ ^self class generality ]
]

SmallInteger members: "copying"
[
    shallowCopy [ ^self ]
    deepCopy [ ^self shallowCopy ]
]

SmallInteger members: "handling errors"
[
    unmodifiableInteger [ "reports an unmodifiable integer"
        self error: 'Attempt to change an unmodifiable SmallInteger instance'. ]
]

SmallInteger members: "manipulating bits - concrete"
[
    @Primitive! bitAnd: SmallInteger! anInteger [
        ^SmallInteger from: self intValue & anInteger intValue ]

    @Primitive! bitOr: SmallInteger! anInteger [
        ^SmallInteger from: self intValue | anInteger intValue ]

    @Primitive! bitXor: SmallInteger! anInteger [
        ^SmallInteger from: (Primitive xorBits: self intValue : anInteger intValue) ]


    @Primitive! bitAt: Ordinal! index [
        @Final! Java Lang Int! x := (Integer <- index) primitiveInteger - 1.
        ((x < 0) | (x > 31)) ifTrue:
            [ (IllegalArgumentException basicNew: 'Bit index out of range, not 1 - 32') throw ].

        @Final! SmallInteger! s := SmallInteger from: (1 << x).
        ^(self bitAnd: s) primitiveInteger == 0
            ifTrue: [ Zero ]
            ifFalse: [ Unity ] ]

    @Primitive! bitAt: Ordinal! index put: Ordinal! bitValue [
        @Final! Java Lang Int! x := (Integer <- index) intValue - 1.
        Java Lang Int! v := (Integer <- bitValue) intValue.
        ((x < 0) | (x > 31)) ifTrue:
            [ (IllegalArgumentException basicNew: 'Bit index out of range, not 1 - 32') throw ].

        @Final! SmallInteger! s := SmallInteger from: (1 << x).
        (v == 0) ifTrue: [ ^SmallInteger from: self intValue & s bitInvert intValue ].
        (v == 1) ifTrue: [ ^SmallInteger from: self intValue | s intValue ].

        (IllegalArgumentException basicNew: 'Bit value must be 0 or 1') throw ]

    @Primitive! bitShift: Ordinal! count [
        (Integer <- count) primitiveInteger < 0 ifTrue: [ ^self rightShift: Integer <- count negated ].
        ^self leftShift: Integer <- count ]
]

SmallInteger members: "bit shifts - primitive"
[
    @Private! @Primitive! rightShift: Integer! count [
        ^SmallInteger from: self intValue >> count intValue ]

    @Private! @Primitive! leftShift: Integer! count [
        ^SmallInteger from: self intValue << count intValue ]
]

SmallInteger members: "testing behavior"
[
    Boolean! isLiteral [ ^true ]
    Boolean! isSmallInteger [ ^true ]
]

SmallInteger "mathematics" members:
[
    abs [ ^self < Zero ifTrue: [ self negated ] ifFalse: [ self ] ]
    @Primitive! negated [ ^SmallInteger from: Zero intValue - self intValue ]
]

SmallInteger "enumerating" members:
[
    timesRepeat: NiladicValuable! aBlock [
        "evaluates aBlock this number of times"
        @Final! FastInteger! count := Unity asFastInteger.
        [ count <= self ] whileTrue: [
            aBlock value.
            count += Unity.
        ].
    ]

    to: SmallInteger! otherBound by: SmallInteger! delta do: MonadicValuable! aBlock [
        "evaluates aBlock with an index, which varies by delta from this value to otherBound"
        @Final! FastInteger! index := self asFastInteger.
        delta > Zero ifTrue: [
            [ index <= otherBound ] whileTrue: [
                aBlock value: index.
                index += delta.
            ]
        ]
        ifFalse: [
            [ otherBound <= index ] whileTrue: [
                aBlock value: index.
                index += delta.
            ]
        ].
    ]
]

SmallInteger "printing" members:
[
    String! printString [ ^String from: (Java Lang Integer toString: self intValue) ]
]
