package Hoot.Compiler;

import org.junit.*;
import static Hoot.Compiler.HootMain.*;

/**
 * Compiles the standard Smalltalk types.
 *
 * @author nik <nikboyd@sonic.net>
 * @see "Copyright 2010,2019 Nikolas S Boyd."
 * @see "Permission is granted to copy this work provided this copyright statement is retained in all copies."
 * @see <a href="https://gitlab.com/hoot-smalltalk/hoot-smalltalk/tree/master/LICENSE.txt">LICENSE for more details</a>
 */
public class CompilerTest {
    
    static final String LibsSmalltalk = "../libs-smalltalk";
    static final String[] SmalltalkPackages = {
        "Smalltalk.Core",
        "Smalltalk.Blocks",
        "Smalltalk.Magnitudes",
        "Smalltalk.Collections",
        "Smalltalk.Streams",
        "Smalltalk.Exceptions",
    };

    @Test public void compileLibrary() { main(buildCleanCommand(LibsSmalltalk, SmalltalkPackages)); }

} // CompilerTest
