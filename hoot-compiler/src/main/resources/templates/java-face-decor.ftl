[#ftl]
[#-- Copyright 2010,2019 Nikolas S Boyd.
Permission is granted to copy this work provided this copyright statement is retained in all copies.
See https://gitlab.com/hoot-smalltalk/hoot-smalltalk/tree/master/LICENSE.txt for more details. --]
/** ${face.signature().comment()} */
[#list face.signature().decor() as note]${note} [/#list]