//==================================================================================================
// Copyright 2010,2019 Nikolas S Boyd.
// Permission is granted to copy this work provided this copyright statement is retained in all copies.
// See https://gitlab.com/hoot-smalltalk/hoot-smalltalk/tree/master/LICENSE.txt for LICENSE details.
//==================================================================================================

grammar Hoot;

@header {
import Hoot.Runtime.Notes.*;
import Hoot.Runtime.Names.*;
import Hoot.Runtime.Values.*;
import Hoot.Runtime.Emissions.Item;
import Hoot.Runtime.Behaviors.Scope;
import static Hoot.Runtime.Functions.Utils.*;

import Hoot.Compiler.Notes.*;
import Hoot.Compiler.Scopes.*;
import Hoot.Compiler.Scopes.File;
import Hoot.Compiler.Scopes.Block;
import Hoot.Compiler.Expressions.*;
import Hoot.Compiler.Constants.*;
}

@members {
static final String Empty = "";
}

compilationUnit : ( notes+=notation )* ( fileImport )* ( classScope | typeScope ) ;

importSelector : ImportOne | ImportAll | ImportStatics ;

fileImport
: g=globalReference m=importSelector Period {
File.currentFile().importFace(Import.from(File.currentFile(), $g.item, $m.text));}
;

typeScope  : sign=typeSignature  ( scopes+=protocolScope )* ;
classScope : sign=classSignature ( scopes+=protocolScope )* ;

typeSignature
: heritage=typeHeritage k=subtypeKeyword ( notes+=notation )*
  subType=globalName details=detailedSignature p=Period {
CompilationUnitContext unit = (CompilationUnitContext)$ctx.getParent().getParent();
Face.currentFace().notes().noteAll(map(unit.notes, n -> n.item));
Face.currentFace().signature(TypeSignature.with(
TypeList.withDetails(map($ctx.heritage.superTypes, t -> t.item)),
DetailedType.with(Global.named($ctx.subType.name), $ctx.details.list),
new NoteList().noteAll(map($ctx.notes, n -> n.item)), $ctx.k.getText(),
Comment.findComment($ctx.heritage.getStart(), $ctx.p)));}
;

classSignature
: heritage=classHeritage k=subclassKeyword ( notes+=notation )* ( types+=detailedType Bang )*
  subClass=globalName details=detailedSignature p=Period {
CompilationUnitContext unit = (CompilationUnitContext)$ctx.getParent().getParent();
Face.currentFace().notes().noteAll(map(unit.notes, n -> n.item));
Face.currentFace().signature(ClassSignature.with(
nullOr(t -> t.item, $ctx.heritage.superClass),
DetailedType.with(Global.named($ctx.subClass.name), $ctx.details.list),
TypeList.withDetails(map($ctx.types, t -> t.item)),
new NoteList().noteAll(map($ctx.notes, n -> n.item)), $ctx.k.getText(),
Comment.findComment($ctx.heritage.getStart(), $ctx.p)));}
;

protocolScope
: sign=protocolSignature b=BlockInit ( members+=classMember )* x=BlockExit
;

protocolSignature returns [String selector = ""]
: ( notes+=notation )* g=globalName s=metaUnary k=membersKeyword {
$selector = $s.text; Face.currentFace().selectFace($selector);}
;

classMember
: v=namedVariable | m=methodScope
;

namedVariable returns [Variable item = null]
: ( notes+=notation )* type=typeNotation v=valueName ( Assign value=expression )? p=Period {
$item = Variable.named($ctx.v.name, nullOr(x -> x.item, $ctx.type), nullOr(x -> x.item, $ctx.value))
.withNotes(map($ctx.notes, n -> n.item))
.defineMember();}
;

//==================================================================================================
// methods + blocks
//==================================================================================================

methodScope returns [Method item = null]
: ( notes+=notation )* sign=methodSignature b=methodBeg ( c=construct Period )? content=blockContent x=methodEnd
;

methodBeg : BlockInit {
$methodScope::item = new Method().makeCurrent();
MethodScopeContext scope = (MethodScopeContext)$ctx.getParent();
$methodScope::item.notes().noteAll(map(scope.notes, n -> n.item));
$methodScope::item.signature(scope.sign.item);}
;

methodEnd : BlockExit {
MethodScopeContext scope = (MethodScopeContext)$ctx.getParent();
$methodScope::item.content(scope.content.item);
$methodScope::item.construct(nullOr(r -> r.item, nullOr(ctx -> ctx.c, scope)));
$methodScope::item.popScope();}
;

methodSignature returns [BasicSignature item = null]
: ksign=keywordSignature | bsign=binarySignature | usign=unarySignature
;

unarySignature
: result=typeNotation name=unarySelector {
$methodSignature::item =
UnarySignature.with(nullOr(t -> t.item, $ctx.result), $ctx.name.selector);}
;

binarySignature
: result=typeNotation name=binaryOperator args+=namedArgument {
$methodSignature::item =
BinarySignature.with(nullOr(t -> t.item, $ctx.result), map($ctx.args, arg -> arg.item), $ctx.name.op);}
;

keywordSignature
: result=typeNotation name=headsAndTails {
$methodSignature::item =
KeywordSignature.with(nullOr(t -> t.item, $ctx.result), $ctx.name.argList, $ctx.name.headList, $ctx.name.tailList);}
;

headsAndTails returns
[List<Variable> argList, List<String> headList, List<String> tailList]
: heads+=keywordHead args+=namedArgument (  |
( heads+=keywordHead args+=namedArgument )+ |
( tails+=keywordTail args+=namedArgument )+ ) {
$argList = map($ctx.args, arg -> arg.item);
$headList = map($ctx.heads, head -> head.selector);
$tailList = map($ctx.tails, tail -> tail.selector);}
;

namedArgument returns [Variable item = null]
: ( notes+=notation )* type=typeNotation v=variableName {
$item = Variable.named($ctx.v.name, nullOr(x -> x.item, $ctx.type))
.withNotes(map($ctx.notes, n -> n.item));}
;

blockScope returns [Block item = null]
: blockBeg sign=blockSignature content=blockContent blockEnd
;

blockBeg : BlockInit {$blockScope::item = new Block().makeCurrent();} ;
blockEnd : BlockExit {
BlockScopeContext scope = (BlockScopeContext)$ctx.getParent();
$blockScope::item.content(scope.content.item);
$blockScope::item.popScope();} 
;

blockSignature returns [KeywordSignature item = null]
: ( | type=typeNotation ( tails+=keywordTail args+=namedArgument )+ Bar ) {
$item = KeywordSignature.with(nullOr(t -> t.item, $ctx.type), map($ctx.args, arg -> arg.item));
$blockScope::item.signature($item);}
;

blockContent returns [BlockContent item = null]
: ( s+=statement p+=Period )* ( s+=statement ( p+=Period )? | r=exitResult | ) {
$item = BlockContent.with(map($ctx.s, term -> term.item), nullOr(ctx -> ctx.item, $ctx.r), $ctx.p.size());}
;

//==================================================================================================
// values + messages
//==================================================================================================

exitResult returns [Expression item = null]
: Exit value=expression {$item = $value.item.makeExit();}
;

statement returns [Statement item = null]
: ( x=assignment | v=evaluation ) {
$item = Statement.with($ctx.v == null ? $ctx.x.item : $ctx.v.item);}
;

construct returns [Construct item = null]
: ref=selfish ( tails+=keywordTail terms+=formula )* {
$item = Construct.with($ref.item, map($terms, term -> term.item));}
;

evaluation returns [Expression item = null]
: value=expression {$item = $value.item.makeEvaluated();}
;

assignment returns [Variable item = null]
: ( notes+=notation )* type=typeNotation v=valueName Assign value=expression {
$item = Variable.named($ctx.v.name, nullOr(x -> x.item, $ctx.type), nullOr(x -> x.item, $ctx.value))
.withNotes(map($ctx.notes, n -> n.item))
.makeAssignment();}
;

primary returns [Primary item = null]
:
( term=nestedTerm       {$item = Primary.with($term.item);}
| block=blockScope      {$item = Primary.with($block.item.withNest());}
| value=literal         {$item = Primary.with($value.item);}
| type=globalReference  {$item = Primary.with($type.item.makePrimary());}
| var=variableName      {$item = Primary.with(LiteralName.with($var.name));}
)
;

nestedTerm returns [Expression item = null]
: TermInit term=expression TermExit {
$item = $ctx.term.item;}
;

expression returns [Expression item = null]
: f=formula ( kmsg=keywordMessage )? ( cmsgs+=messageCascade )* {
$item = Expression.with($ctx.f.item, nullOr(m -> m.item, $ctx.kmsg), map($ctx.cmsgs, msg -> msg.m.item));}
;

formula returns [Formula item = null]
: s=unarySequence ( ops+=binaryMessage )* {
$item = Formula.with($ctx.s.item, map($ctx.ops, op -> op.item));}
;

unarySequence returns [UnarySequence item = null]
: p=primary ( msgs+=unarySelector )* {
$item = UnarySequence.with($ctx.p.item, map($ctx.msgs, m -> m.selector));}
;

binaryMessage returns [BinaryMessage item = null]
: operator=binaryOperator term=unarySequence {
$item = BinaryMessage.with($ctx.operator.op, Formula.with($ctx.term.item));}
;

keywordMessage returns [KeywordMessage item = null]
:   heads+=keywordHead terms+=formula
( ( heads+=keywordHead terms+=formula )+
| ( tails+=keywordTail terms+=formula )+
| ) {
List<String> heads = map($ctx.heads, head -> head.selector);
List<String> tails = map($ctx.tails, tail -> tail.selector);
List<Formula> terms = map($ctx.terms, term -> term.item);
$item = KeywordMessage.with(heads, tails, terms);}
;

messageCascade : Cascade m=message ;

message returns [Message item = null]
: kmsg=keywordMessage {$item = $kmsg.item;} # KeywordSelection
| bmsg=binaryMessage  {$item = $bmsg.item;} # BinarySelection
| umsg=unarySelector  {$item = UnarySequence.with($umsg.selector);} # UnarySelection
;

//==================================================================================================
// notations
//==================================================================================================

notation returns [KeywordNote item = null]
: At name=globalName ( ( values+=namedValue )+ | ( nakeds+=nakedValue )+ )? Bang {
$item = KeywordNote.with($ctx.name.name, map($ctx.nakeds, n -> n.item), map($ctx.values, n -> n.item));}
;

namedValue returns [NamedValue item = null]
: head=keywordHead v=primitive  {$item = NamedValue.with($head.text, $v.item);}
| head=keywordHead g=globalName {$item = NamedValue.with($head.text, Global.with($g.name));}
;

nakedValue returns [NamedValue item = null]
: tail=keywordTail v=primitive  {$item = NamedValue.with(Empty, $v.item);}
| tail=keywordTail g=globalName {$item = NamedValue.with(Empty, Global.with($g.name));}
;

typeHeritage  : Nil | superTypes+=detailedType ( Comma superTypes+=detailedType )* ;
classHeritage : Nil | superClass=signedType ;

typeNotation returns [DetailedType item = null]
: ( | type=detailedType Bang ( etc=Etc )? ) {
$item = nullOr(t -> t.item.makeArrayed($ctx.etc != null), $ctx.type);}
;

detailedSignature returns [TypeList list = null]
: ( generics=genericTypes )? ( Exit exit=detailedType )? {
DetailedType exit = nullOr(x -> x.item, $ctx.exit);
TypeList list = nullOr(g -> g.list, $ctx.generics);
if (list == null) list = new TypeList();
$list = list.withExit(exit);}
;

detailedType returns [DetailedType item = null]
: extent=extendType {$item = $extent.item;}
| signed=signedType {$item = $signed.item;}
;

genericTypes returns [TypeList list = null]
: Quest types+=detailedType ( keywordTail types+=detailedType )* {
$list = TypeList.withDetails(map($types, t -> t.item));}
;

extendType returns [DetailedType item = null]
: g=globalName Extends baseType=detailedType {
$item = DetailedType.with(Global.with($g.name), $baseType.item).makeExtensive();}
;

signedType returns [DetailedType item = null]
: g=globalReference details=detailedSignature {
$item = DetailedType.with($g.item, $details.list);}
;

//==================================================================================================
// references
//==================================================================================================

literalNil     : Nil   ;
literalSelf    : Self  ;
literalSuper   : Super ;
literalBoolean : True | False ;

valueName returns [String name = Empty]
: ( v=variableName {$name = $v.name;}
  | g=globalName   {$name = $g.name;}
  )
;

variableName returns [String name = Empty]
: v=LocalName  {$name = $v.text;}
;

globalName returns [String name = Empty]
: g=GlobalName {$name = $g.text;}
;

globalReference returns [Global item = null]
: ( names+=globalName )+  {$item = Global.withList(map($names, n -> n.name));}
;

//==================================================================================================
// keywords
//==================================================================================================

subclassKeyword
: Subclass {File.currentFile().faceScope().makeCurrent();}
;

subtypeKeyword
: Subtype  {File.currentFile().faceScope().makeCurrent();}
;

metaclassKeyword  : Metaclass ;
metatypeKeyword   : Metatype ;
classKeyword      : Class ;
typeKeyword       : Type ;

implementsKeyword : Implements ;
protocolKeyword   : Protocol ;
membersKeyword    : Members ;

reservedWord
: Metaclass  | Subclass | Class
| Metatype   | Subtype  | Type
| Implements | Protocol | Members
| ImportOne  | ImportAll | ImportStatics
;

keywordHead returns [String selector = Empty]
: head=KeywordHead  {$selector = $head.text;}
| word=reservedWord {$selector = $word.text;}
;

keywordTail returns [String selector = Empty]
: tail=KeywordTail  {$selector = $tail.text;}
;

//==================================================================================================
// selectors
//==================================================================================================

metaUnary  : classUnary | typeUnary | ;
classUnary : ClassUnary ;
typeUnary  : TypeUnary ;

unarySelector returns [String selector = Empty]
: ( s=LocalName | s=ClassUnary | s=TypeUnary | s=GlobalName ) {
$selector = Keyword.with($s.text).methodName();}
;

binaryOperator returns [Operator op]
: ( s=At | s=Bar | s=Comma | s=BinaryOperator | s=Usage ) {
$op = Operator.with($s.text);}
;

//==================================================================================================
// constants
//==================================================================================================

primitiveValues returns [LiteralArray list = null]
: Pound TermInit ( array+=primitive )* TermExit {
$list = LiteralArray.withItems(map($array, v -> v.item));}
;

elementValues returns [LiteralArray list = null]
: Pound TermInit ( array+=elementValue )* TermExit {
$list = LiteralArray.withItems(map($array, v -> v.item));}
;

elementValue returns [Constant item = null]
: lit=literal      {$item = $lit.item;}
| var=variableName {$item = LiteralName.with($var.text, $start.getLine());}
;

primitive returns [Constant item = null]
: array=primitiveValues   {$item = $array.list;}
| bool=literalBoolean     {$item = LiteralBoolean.with($start.getText(), $start.getLine());}
| value=ConstantCharacter {$item = LiteralCharacter.with($start.getText(), $start.getLine());}
| value=ConstantInteger   {$item = LiteralInteger.with($start.getText(), $start.getLine());}
| value=ConstantFloat     {$item = LiteralFloat.with($start.getText(), $start.getLine());}
| value=ConstantSymbol    {$item = LiteralSymbol.with($start.getText(), $start.getLine());}
| value=ConstantString    {$item = LiteralString.with($start.getText(), $start.getLine());}
;

selfish returns [Constant item = null;]
: refSelf=literalSelf     {$item = LiteralName.with($refSelf.text, $start.getLine());}
| refSuper=literalSuper   {$item = LiteralName.with($refSuper.text, $start.getLine());}
;

literal returns [Constant item = null]
: array=elementValues     {$item = $array.list;}
| refNil=literalNil       {$item = LiteralNil.with($start.getText(), $start.getLine());}
| refSelf=literalSelf     {$item = LiteralName.with($refSelf.text, $start.getLine());}
| refSuper=literalSuper   {$item = LiteralName.with($refSuper.text, $start.getLine());}
| bool=literalBoolean     {$item = LiteralBoolean.with($start.getText(), $start.getLine());}
| value=ConstantCharacter {$item = LiteralCharacter.with($start.getText(), $start.getLine());}
| value=ConstantDecimal   {$item = LiteralDecimal.with($start.getText(), $start.getLine());}
| value=ConstantFloat     {$item = LiteralFloat.with($start.getText(), $start.getLine());}
| value=ConstantInteger   {$item = LiteralInteger.with($start.getText(), $start.getLine());}
| n=radixedNumber         {$item = LiteralRadical.with($start.getText(), $start.getLine());}
| value=ConstantSymbol    {$item = LiteralSymbol.with($start.getText(), $start.getLine());}
| value=ConstantString    {$item = LiteralString.with($start.getText(), $start.getLine());}
;

//==================================================================================================
// scopes
//==================================================================================================

BlockInit : '[' ;
BlockExit : ']' ;

TermInit  : '(' ;
TermExit  : ')' ;

NoteInit  : '{' ;
NoteExit  : '}' ;

//==================================================================================================
// punctuators
//==================================================================================================

Assign  : Colon Equal ;
Extends : Minus More ;
Usage   : Less Minus ;
Etc     : '...' ;

Exit    : '^' ;
Cascade : ';' ;
Bang    : '!' ;
Quest   : '?' ;
Pound   : '#' ;
Comma   : ',' ;
Bar     : '|' ;
At      : '@' ;

//==================================================================================================
// literal numbers
//==================================================================================================

radixedNumber  : ConstantBinary | ConstantOctal | ConstantHex ;

ConstantBinary  : BinaryRadix  BinaryDigit+ ;
ConstantOctal   : OctalRadix   OctalDigit+ ;
ConstantHex     : HexRadix     HexDigit+ ;

ConstantDecimal : CardinalNumber ( Dot CardinalFraction )? Scale ;
ConstantFloat   : CardinalNumber Dot CardinalFraction Exponent? | CardinalNumber Exponent ;
ConstantInteger : CardinalNumber ;

Period : Dot ;

fragment OrdinaryNumber   : OrdinalDigit DecimalDigit* ;
fragment OrdinaryFraction : DecimalDigit* OrdinalDigit ;
fragment CardinalNumber   : Zero | OrdinaryNumber ;
fragment CardinalFraction : Zero | OrdinaryFraction ;

fragment Dot      : '.' ;
fragment Scale    : ScaleMark OrdinaryNumber ;
fragment Exponent : PowerMark Minus? OrdinaryNumber ;

fragment BinaryRadix :  '2r' ;
fragment OctalRadix  :  '8r' ;
fragment HexRadix    : '16r' ;
fragment ScaleMark   :   's' ;
fragment PowerMark   :   'e' ;

//==================================================================================================
// keywords + identifiers
//==================================================================================================

Nil         : 'nil' ;
Self        : 'self' ;
Super       : 'super' ;
True        : 'true' ;
False       : 'false' ;

Metaclass   : 'metaclass:' ;
Subclass    : 'subclass:' ;
Class       : 'class:' ;
ClassUnary  : 'class' ;

Metatype    : 'metatype:' ;
Subtype     : 'subtype:' ;
Type        : 'type:' ;
TypeUnary   : 'type' ;

Implements  : 'implements:' ;
Protocol    : 'protocol:' ;
Members     : 'members:' ;

ImportOne   : 'import' ;
ImportAll   : 'importAll' ;
ImportStatics : 'importStatics' ;

KeywordHead : Name Colon ;
KeywordTail : Colon ;
GlobalName  : UpperCase Tail* ;
LocalName   : LowerCase Tail* ;

fragment Colon  : ':' ;
fragment Name   : Letter Tail* ;
fragment Tail   : Letter | DecimalDigit ;
fragment Letter : UpperCase | LowerCase ;

//==================================================================================================
// strings
//==================================================================================================

CodeComment       : QuotedComment -> channel(HIDDEN) ;

ConstantCharacter : '$' . ;
ConstantSymbol    : Pound SymbolString ;
ConstantString    : QuotedString ConstantString? ;

fragment QuotedString  : SingleQuote .*? SingleQuote ;
fragment QuotedComment : DoubleQuote .*? DoubleQuote ;

fragment SymbolString :
    KeywordHead+ KeywordTail* |
    ConstantString |
    BinaryOperator |
    Name ;

fragment DoubleQuote : '\"' ;
fragment SingleQuote : '\'' ;

//==================================================================================================
// operators
//==================================================================================================

BinaryOperator :
  ComparisonOperator | MathOperator | LogicalOperator | ExtraOperator | FastMath | ShiftOperator ;

fragment ComparisonOperator :
  More | Less | Equal | More Equal | Less Equal | Not Equal | Equal Equal | Not Not | Equal Equal Equal ;

fragment LogicalOperator : And | Or | Less Less | More More ;
fragment MathOperator    : Times | Times Times | Divide | Divide Divide | Modulo | Plus | Minus ;
fragment FastMath        : Plus Equal | Minus Equal | Times Equal | Divide Equal ;
fragment ExtraOperator   : Percent | Quest ;
fragment ShiftOperator   : More More | Less Less | Less Less Equal | Exit Equal ;

//==================================================================================================
// operators
//==================================================================================================

fragment Percent  : '%' ;

fragment Truncate : '//' ;
fragment Modulo   : '\\\\' ;
fragment Divide   : '/' ;
fragment Times    : '*' ;
fragment Plus     : '+' ;
fragment Minus    : '-' ;

fragment Equal    : '=' ;
fragment More     : '>' ;
fragment Less     : '<' ;
fragment Not      : '~' ;

fragment And      : '&' ;
fragment Or       : '|' ;

//==================================================================================================
// letters + digits
//==================================================================================================

fragment UpperCase : [A-Z] ;
fragment LowerCase : [a-z] ;

fragment DecimalDigit : [0-9] ;
fragment OrdinalDigit : [1-9] ;
fragment BinaryDigit  : [0-1] ;
fragment OctalDigit   : [0-7] ;
fragment HexDigit     : DecimalDigit | [A-F] ;
fragment Zero         : '0' ;

//==================================================================================================
// white space
//==================================================================================================

WhiteSpaces : WhiteSpace+ -> skip ;
fragment WhiteSpace : [ \t\r\n\f] ;

//==================================================================================================

