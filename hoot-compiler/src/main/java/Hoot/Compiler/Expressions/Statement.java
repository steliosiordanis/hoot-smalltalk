package Hoot.Compiler.Expressions;

import Hoot.Runtime.Emissions.*;
import Hoot.Runtime.Faces.Resulting;
import Hoot.Runtime.Values.Operand;
import Hoot.Runtime.Behaviors.Scope;
import Hoot.Compiler.Scopes.*;

/**
 * A statement (or result).
 *
 * @author nik <nikboyd@sonic.net>
 * @see "Copyright 1999,2019 Nikolas S Boyd."
 * @see "Permission is granted to copy this work provided this copyright statement is retained in all copies."
 * @see <a href="https://gitlab.com/hoot-smalltalk/hoot-smalltalk/tree/master/LICENSE.txt">LICENSE for more details</a>
 */
public class Statement extends Item implements ScopeSource, Resulting {

    public Statement(Item parent) { super(parent); }
    protected Statement(Operand item) { this(Scope.current()); this.item = item.inside(this); }
    public static Statement with(Operand item) { if (item == null) return null; return new Statement(item); }
    @Override public void clean() { super.clean(); this.item.clean(); }

    protected Operand item;
    public boolean isConstruct() { return item.isConstruct(); }
    @Override public boolean isExit() { return item.isExit(); }
    @Override public boolean containsExit() { return item.containsExit(); }
    @Override public boolean isResult() { return item.isResult(); }
    @Override public boolean isFramed() { Method m = method(); return m != null && m.needsFrame(); }

    public boolean throwsException() { return this.item.throwsException(); }
    public Statement makeResult() { item.makeResult(); return this; }

    @Override public Emission emitItem() { return this.isResult() ? emitValue() : emitStatement(); }
    public Emission emitValue() {
        if (this.isResult()) {
            return this.isFramed() ?
                // only exits are framed
                emitExit(emitOperand()) :
                emitResult();
        }
        return emitOperand();
    }

    private Emission emitStatement() {
        if (method().isPrimitive()) {
            return this.item.needsStatement() ?
                this.item.emitStatement(this.item.emitPrimitive()) :
                this.item.emitPrimitive();
        }

        return this.item.emitStatement(); }

    private Emission emitResult() {
        if (this.item.hasCascades()) {
            return this.item.emitOperand();
        }
        return method().isPrimitive() ?
                this.item.emitResult(block().emitResultType(), this.item.emitPrimitive()) :
                this.item.emitResult(block().emitResultType(), this.item.emitOperand()); }

    private Emission emitOperand() {
        return method().isPrimitive() ?
                this.item.emitPrimitive() :
                this.item.emitOperand(); }

} // Statement
