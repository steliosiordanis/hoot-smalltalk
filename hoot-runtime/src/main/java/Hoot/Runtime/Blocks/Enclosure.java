package Hoot.Runtime.Blocks;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;

import Hoot.Runtime.Faces.*;
import Hoot.Runtime.Values.Frame;
import Hoot.Runtime.Behaviors.HootRegistry;
import static Hoot.Runtime.Functions.Utils.*;
import Hoot.Runtime.Functions.*;
import static Hoot.Runtime.Functions.Exceptional.*;
import static Hoot.Runtime.Functions.Exceptional.Result.*;
import static Hoot.Runtime.Names.Primitive.normalPriority;

/**
 * A block enclosure.
 *
 * <h4>Enclosure Responsibilities:</h4>
 * <ul>
 * <li>knows a block</li>
 * <li>knows a block argument count</li>
 * <li>evaluates a block to produce a result</li>
 * </ul>
 *
 * @author nik <nikboyd@sonic.net>
 * @see "Copyright 2010,2019 Nikolas S Boyd."
 * @see "Permission is granted to copy this work provided this copyright statement is retained in all copies."
 * @see <a href="https://gitlab.com/hoot-smalltalk/hoot-smalltalk/tree/master/LICENSE.txt">LICENSE for more details</a>
 */
public class Enclosure implements NiladicValuable, MonadicValuable, DyadicValuable {

    protected Enclosure(Function<Frame,?> block) { this.block = block; }

    public static Enclosure withBlock(Consumer<Frame> block) {
        if (block == null) throw new IllegalArgumentException("block missing");
        return new Enclosure(f -> { block.accept(f); return null; }); }

    public static Enclosure withBlock(Function<Frame,?> block) {
        if (block == null) throw new IllegalArgumentException("block missing");
        return new Enclosure(block); }

    public static Enclosure withQuiet(Argued<Frame,?> block) {
        if (block == null) throw new IllegalArgumentException("block missing");
        return new Enclosure(f -> Exceptional.nullOrTryQuietly(block, f)); }

    public static Enclosure withQuieted(Runner block) {
        if (block == null) throw new IllegalArgumentException("block missing");
        return new Enclosure(f -> { Exceptional.runQuietly(block); return null; }); }

    private final Function<Frame,?> block;
    public <ValueType> Function<Frame,ValueType> block() { return (Function<Frame,ValueType>)this.block; }

    private Frame frame = new Frame();
    public Frame frame() { return this.frame; }
    public Enclosure withFrame(Frame frame) { this.frame = frame; return this; }
    @Override public IntegerValue argumentCount() { return IntegerValue.Source.with(frame().countArguments()); }
    
    public Enclosure runQuiet() { Exceptional.runQuietly(() -> value()); return this; }
    public Enclosure runLoud()  { Exceptional.runLoudly(() -> value()); return this; }

    /**
     * @return a value resulting from the base block, after the termination block is also evaluated
     * @param terminationBlock always evaluated (finally)
     */
    public Valued ensure(Valuable terminationBlock) {
        return defaultOrTryQuietly(() -> value(), HootRegistry.Nil(), () -> terminationBlock.value()); }
    
    public Enclosure $catch(MonadicValuable terminationBlock) {
        runLoudly(() -> value(), caughtBy(terminationBlock)); return this; }
    
    private Handler<Throwable> caughtBy(MonadicValuable terminationBlock) {
        return (Throwable ex) -> { terminationBlock.value(ex); }; }
    
    public Valued defaultIfCurtailed(Valued defaultValue) {
        return defaultOrTrySurely(() -> value(), DebugHandler, defaultValue); }

    /**
     * @return a value resulting either from the base block, or the termination block if the base fails
     * @param terminationBlock evaluated only if an exception occurs (catch)
     */
    public Valued ifCurtailed(Valuable terminationBlock) {
        return defaultOrTrySurely(() -> value(), curtailment(terminationBlock), HootRegistry.Nil()); }
    
    private Handler<Throwable> curtailment(Valuable terminationBlock) {
        return (Throwable ex) -> { terminationBlock.value(); }; }

    public <V> boolean testWithEach(Set<V> values) { return values.stream().anyMatch(each -> value(each)); }
    public <V> Set<?>  evaluateWithEach(Set<V> values) { return mapSet(values, each -> value(each)); }
    public <V> List<?> evaluateWithEach(V ... values) { return evaluateWithEach(wrap(values)); }
    public <V> List<?> evaluateWithEach(List<V> values) { return map(values, each -> value(each)); }

    @Override public <R> R value() { return (R)block().apply(frame()); }
    @Override public <V, R> R value(V value) { return valueWith(value); }
    @Override public <A, B, R> R value_value(A a, B b) { return valueWith(a, b); }
    
    static final Object Placeholder = new Object();
    public Enclosure valueNames(String... valueNames) { return valueNames(wrap(valueNames)); }
    public Enclosure valueNames(List<String> valueNames) {
        valueNames.forEach(vn -> frame().bind(vn, Placeholder)); return this; }

    public <R> R valueWith(Object... values) {
        return nullOrTryLoudly(
                () -> { frame().withAll(values); return value(); }, 
                () -> { frame().purge(); }); }
    
    public Thread fork() { return forkAt(normalPriority()); }
    public Thread forkAt(IntegerValue priority) { return forkAt(priority.intValue()); }
    public Thread forkAt(int priority) {
        Thread result = new Thread(() -> value());
        result.setPriority(priority);
        result.start();
        return result; }

} // Enclosure
