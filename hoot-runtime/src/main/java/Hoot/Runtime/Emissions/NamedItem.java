package Hoot.Runtime.Emissions;

import java.util.*;
import org.apache.commons.lang3.StringUtils;

import Hoot.Runtime.Faces.Named;
import Hoot.Runtime.Behaviors.Typified;
import static Hoot.Runtime.Names.Primitive.SerializedTypes;

/**
 * A named item.
 *
 * @author nik <nikboyd@sonic.net>
 * @see "Copyright 2010,2019 Nikolas S Boyd."
 * @see "Permission is granted to copy this work provided this copyright statement is retained in all copies."
 * @see <a href="https://gitlab.com/hoot-smalltalk/hoot-smalltalk/tree/master/LICENSE.txt">LICENSE for more details</a>
 */
public abstract class NamedItem extends Item implements Named {

    public NamedItem(Item container) { super(container); }
    public NamedItem(NamedItem container) { super(container); }
    @Override public String name() { return getClass().getSimpleName(); }

    public NamedItem metaSignature() { return null; }
    public boolean isInterface() { return false; }

    public String comment() { return Empty; }
    public String baseName() { return Empty; }

    public boolean hasHeritage() { return !hasNoHeritage(); }
    public boolean hasNoHeritage() { return simpleHeritage().isEmpty() && typeHeritage().isEmpty(); }

    public List<Typified> simpleHeritage() { return new ArrayList(); }
    public List<Typified> typeHeritage() { return new ArrayList(); }

    public void reportUnknown(String name) {
        if (StringUtils.isEmpty(name)) return;
        if (SerializedTypes.contains(name)) return;

        String fullName = fullName();
        if (fullName().startsWith("Samples.")) return;
        if (fullName().equals("Hoot.Magnitudes.Character")) return;
        logger().warn(fullName + " can't find " + name);
    }

} // NamedItem
