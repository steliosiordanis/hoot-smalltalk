package Hoot.Runtime.Values;

import Hoot.Runtime.Faces.Valued;

/**
 * A method exit with a value.
 *
 * @author nik <nikboyd@sonic.net>
 * @see "Copyright 2010,2019 Nikolas S Boyd."
 * @see "Permission is granted to copy this work provided this copyright statement is retained in all copies."
 * @see <a href="https://gitlab.com/hoot-smalltalk/hoot-smalltalk/tree/master/LICENSE.txt">LICENSE for more details</a>
 */
public class Exit extends RuntimeException implements Valued {

    protected Exit(String scope, Valued value) { this.value = value; this.scope = scope; }
    public static Exit with(String scope, Valued value) { return new Exit(scope, value); }

    protected String scope = "";
    public String scope() { return this.scope; }
    public boolean exits(Frame f) { return this.scope.equals(f.scope()); }

    protected Valued value = null;
    @Override public Valued value() { return this.value; }

    public static final String FrameIdType = "java.lang.String";
    public static final String FrameId = "exitID";

} // Exit
