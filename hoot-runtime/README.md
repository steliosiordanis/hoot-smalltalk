### Hoot Runtime Library

Contains the Hoot runtime library classes and tests.

| **Package** | **Contents** |
| ----------- | ------------ |
| Hoot Runtime Behaviors | `HootRegistry` and reflective behaviors |
| Hoot Runtime Blocks | `Enclosure` base class for closures and predicates |
| Hoot Runtime Emissions | compiler support base classes |
| Hoot Runtime Exceptions | `ExceptionBase` to integrate derived Hoot exceptions with Java |
| Hoot Runtime Maps | mapping classes for paths, zips, method caches, packages |
| Hoot Runtime Names | mapping classes for names and selectors |
| Hoot Runtime Notes | mapping classes for annotations and decorations |
| Hoot Runtime Values | mapping classes for stack frames and value stores |


```
Copyright 2010,2019 Nikolas S Boyd.
Permission is granted to copy this work provided this copyright statement is retained in all copies.
```
See https://gitlab.com/hoot-smalltalk/hoot-smalltalk/tree/master/LICENSE.txt for LICENSE details.
